Framework de Sistema de L�gica Nebulosa Tipo-2 Intervalar para MATLAB(R)
==============================================================================

Framework desenvolvido por Ciro Esteves Lima Sobral durante o curso de Mestrado
no Programa de P�s-Gradua��o em Inform�tica (PPGI) na Universidade Federal do
Rio de Janeiro (UFRJ).

Este framework propiciou a realiza��o dos testes de um classificador usando
l�gica nebulosa tipo-2 intervalar, para o diagn�stico de problemas em motores
� diesel atrav�s da an�lise de �leos lubrificantes, necess�rios para compara��o
entre classificadores usando l�gica nebulosa tradicional e os classificadores
usando l�gica nebulosa tipo-2 intervalar.

Os resultados desse trabalho foram usados para conclus�o da disserta��o
apresentada como parte dos requisitos necess�rios do t�tulo de Mestre em
Inform�tica, com orienta��o de Adriano Joaquim de Oliveira Cruz e Co-orienta��o
de Antonio Carlos Gay Thom�.

�ndice
------

**Fun��es**

* [newit2fis](#newit2fis)
* [isit2fis](#isit2fis)
* [it2trapmf](#it2trapmf)
* [it2gaussmf](#it2gaussmf)
* [mf2it2mf](#mf2it2mf)
* [fis2it2fis](#fis2it2fis)
* [plotIt2Mf](#plotIt2Mf)
* [plotIt2Var](#plotIt2Var)
* [it2wtaver](#it2wtaver)
* [it2flsengine](#it2flsengine)
* [evalit2fis](#evalit2fis)

------------------------------------------------------------------------------

#### newit2fis

Cria a estrutura do sistema de infer�ncia nebuloso tipo-2 intervalar (IT2FIS),
contendo os par�metros do sistema;

#### isit2fis

Checa se uma estrutura cont�m os par�metros de defini��o de um IT2FIS;

#### it2trapmf

Retorna os valores superiores e inferiores da fun��o de pertin�ncia trapezoidal
tipo-2 intervalar. Permite usar centros iguais e modelar fun��o de pertin�ncia
triangular;

#### it2gaussmf

Retorna os valores superiores e inferiores da fun��o de pertin�ncia gaussiana
tipo-2 intervalar. Permite usar incerteza na m�dia ou no desvio;

#### mf2it2mf

Aplica uma mancha de incerteza sobre uma fun��o de pertin�ncia tradicional
e retorna os par�metros da fun��o resultante;

#### fis2it2fis

Define um IT2FIS com base num sistema de infer�ncia nebuloso (FIS) tradicional,
aplicando a fun��o mf2it2mf nas fun��es de pertin�ncia das vari�veis
de entrada e de sa�da do FIS;

#### plotIt2Mf

Plota uma fun��o de pertin�ncia tipo-2 intervalar;

#### plotIt2Var

Representa graficamente todas as fun��es de pertin�ncia de uma ou mais entradas
ou da sa�da de um IT2FIS;

#### it2wtaver

Computa os centroides da esquerda e da direita de um conjunto nebuloso tipo-2
intervalar;

#### it2flsengine

Realiza os c�lculos de um IT2FIS, fuzzificando as entradas, aplicando as implica��es
e agrega��es definidas no conjunto de regras e realizando a tipo-redu��o
e defuzzifica��o;

#### evalit2fis

Checa se as entradas fornecidas s�o v�lidas e fornece os dados para a fun��o
it2flsengine calcular os resultados.