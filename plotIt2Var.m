function h = plotIt2Var(fis, varType, varIndex, numofpoints, entrada)
%PLOTIT2VAR Plota as fun��es de pertin�ncia de uma vari�vel do sistema nebuloso tipo-2 intervalar.
%
%   Representa graficamente todas as fun��es de pertinencia de uma ou mais
%   vari�veis de entrada ou sa�da de um sistema de inferencia nebuloso
%   Tipo-2 Intervalar.
%
%   PLOTIT2VAR(FIS, VARTYPE, [VARINDEX] [NUMOFPOINTS] [ENTRADA]):
%       FIS: Sistema de inferencia nebuloso Tipo-2 Intervalar.
%       VARTYPE: Identifica o tipo de vari�veis a serem plotadas:
%           'input': Plota as vari�veis da entrada.
%           'output': Plota as vari�veis da sa�da.
%       VARINDEX (opcional): Escolhe as vari�veis a serem plotadas. Quando
%       n�o especificado, plota todas.
%       NUMOFPOINTS (opcional): Especifica o n�mero de pontos para
%       discretiza��o das fun��es de pertinencia.
%       ENTRADA (opcional): Apenas para vari�veis de entrada! Plota uma
%       determinada entrada a o grau de pertinencia em cada fun��o de
%       pertinencia para cada vari�vel.
%
%   See also PLOTMF, PLOTIT2MF.

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/07/29 $

    if ~strcmp(varType,'input') && (~strcmp(varType,'output') || ...
            strcmp(fis.type,'it2s'))
        error('No plots for Singleton Output MFs')
    elseif strcmp(varType, 'input')
        vars = fis.input;
    else
        vars = fis.output;
    end
    
    numVars = length(vars);
    ranges = reshape(extractfield(vars, 'range'), 2, numVars);
    
    if nargin < 3 || isempty(varIndex)
        varIndex = 1:numVars;
    end
    
    if nargin < 4
        numofpoints = 101;
    end
    
    h = figure;
    
    fuzzyInput = fuzzifica(fis, [], numofpoints);
    
    if nargin == 5
        entradaFuzzy = fuzzifica(fis, entrada, numofpoints);
    end
    
    col = ceil(numel(varIndex) / 3);
    lin = ceil(numel(varIndex) / col);
    
    colors = colormap;
    numColors = length(colors);
    
    for i = 1:numel(varIndex)
        mfs = length(vars(varIndex(i)).mf); % Encontra o numero de func�es
        
        x = linspace(ranges(1,varIndex(i)), ranges(2,varIndex(i)), ...
            numofpoints);
        
        subplot(lin, col, i);
        
        for j = 1:mfs
            xaxis = x(:);
            yaxis = fuzzyInput(:,:,j,varIndex(i));
            upper = yaxis(:,1);
            lower = yaxis(:,2);

            xfill = [xaxis; flipud(xaxis)];
            yfill = [upper; flipud(lower)];
            
            h = patch(xfill,yfill, 'w', ...
                'DisplayName', vars(varIndex(i)).mf(j).name);
            
            centerIndex=find(upper == max(upper));
            centerIndex=floor(mean(centerIndex));
            text(xaxis(centerIndex), 1.05, vars(varIndex(i)).mf(j).name,...
                'HorizontalAlignment','center', ...
                'VerticalAlignment','middle', ...
                'FontSize',10)
            
            set(h,'FaceColor','flat', 'CData', j, 'CDataMapping','scaled')
            set(h,'facealpha', 0.8);
        end
        
        ylim([-0.1 1.1]);
        ylabel('Degree of membership','FontSize',10)

        grid on
        
        if nargin == 5 && strcmp(varType,'input')
            % Linha horizontal
            line([entrada(varIndex(i)) entrada(varIndex(i))], ...
                [0 max(max(entradaFuzzy(:,:,:,varIndex(i))))], ...
                'DisplayName', 'X', 'Color', 'm')
            
            % Linhas verticais
            for j = 1:mfs
                if entradaFuzzy(:,1,j,varIndex(i)) > 0
                    line([0; entrada(varIndex(i))], ...
                        [entradaFuzzy(:,1,j,varIndex(i)) ...
                        entradaFuzzy(:,1,j,varIndex(i))], ...
                        'DisplayName', ...
                        [vars(varIndex(i)).mf(j).name ' max'], ...
                        'Color', 'b');
                end
                
                if entradaFuzzy(:,2,j,varIndex(i)) > 0
                    line([0; entrada(varIndex(i))], ...
                        [entradaFuzzy(:,2,j,varIndex(i)) ...
                        entradaFuzzy(:,2,j,varIndex(i))], ...
                        'DisplayName', ...
                        [vars(varIndex(i)).mf(j).name ' min'], ...
                        'Color', 'r');
                end
            end
        end
        
        hold off
        
        xlabel(vars(varIndex(i)).name,'FontSize',10);
        xlim(vars(varIndex(i)).range)
    end
end