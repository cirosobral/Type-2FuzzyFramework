function[outextreme,count,theta] = it2wtaver(ypoint,lower,upper,maxflag)
%IT2WTAVER  Computes the maximum and minimum of a weighted average for an interval type-2 fuzzy set.
%
%   IT2WTAVER(YPOINT, LOWER, UPPER, MAXFLAG) recebe os vetores
%   M-dimensionais YPOINT, LOWER e UPPER e retorna o centroide da esquerda
%   (caso MAXFLAG < 0), ou da direita (caso contrário).

%   Copyright 1998 Nilesh N. Karnik
%   $Author: Nilesh N. Karnik $  $Date: 1998/08/09 $
%   $Modified by: Ciro Sobral $  $Date: 2014/09/29 $

% ======================
% Original Documentation
% ======================
% Function used to implement the iterative procedure described in Theorem
% 9-1 to compute the maximum and minimum of a weighted average, where the
% "y_i" s are crisp numbers and the "w_i"s are interval sets that take
% values from some real interval "[w_lower,w_upper]".

% Outputs : "outextreme" is the extreme value of the output. "count" is the
% number of iterations required to reach the optimum, and "theta" is the
% combination of "w_l"s that achieves the optimum.
 
% Inputs : "ypoint", "lower" and "upper" are all M-dimensional vectors.
% "ypoint" contains the "y_l"s. "lower" and "upper" contain, respectively,
% the "w_lower" and "w_upper" values for each weight "w_l". If "maxflag >
% 0" (scalar), "S" is maximized, else it is minimized.

%[ysort,lower_sort,upper_sort] = trimvec(ypoint,lower,upper,1) ;
[ysort, order] = sort(ypoint);
lower_sort = lower(order);
upper_sort = upper(order);

ly=length(ysort) ;

hl = (lower_sort+upper_sort)/2 ;
S = sum(ysort.*hl)/sum(hl) ;   % starting point

eps = 1e-5 ; % small quantity to avoid floating point equality problems

count = 0 ;
theta = hl ;
S_new = S + 10*eps ;

if ((abs(S-ysort(1)) < eps) || (abs(S-ysort(ly)) < eps)),
   outextreme = S ;

else

   while (abs(S-S_new) > eps) ,
      count = count + 1;

      if count > 1,
         S = S_new ;
      end   %%% if count

      in1 = find(ysort > (S-eps)) ;
      
      min1 = min(in1) ;

      if min1(1) > 2,
         in2 = 1 : min1-1 ;
      else in2 = 1;
      end   %%% if
   
      if maxflag > 0,
         theta(in1) = upper_sort(in1) ;
         theta(in2) = lower_sort(in2) ;
      else
         theta(in1) = lower_sort(in1) ;
         theta(in2) = upper_sort(in2) ;

                     % To avoid division by zero if all lower_sort=0
         if abs(S - ysort(min1(1))) < eps,
                theta(min1) = upper_sort(min1) ; 
         end  %%% if abs(S_new ...

      end    %%% if maxflag
   
   
      S_new = sum(ysort.*theta)/sum(theta) ;

   end    %%% while

   outextreme = S_new ;

end % if ((abs(S-ysort(1)) < eps) .......

return ;
