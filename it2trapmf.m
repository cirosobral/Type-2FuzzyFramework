function y = it2trapmf(x, params)
%IT2TRAPMF Trapezoidal membership function.
%
%   IT2TRAPMF(X, PARAMS) returns a matrix which is the Interval Type-2
%   trapezoidal membership function evaluated at X. PARAMS = [A B C D E F G
%   H I] is a 9-element vector that determines the break points of this
%   membership function. It can also be used to return Interval Type-2
%   triangular membership function by using B = C and F = G.
%
%   IT2TRAPMF(X, [A B C D E F G H I]);
%       X: Input vector
%       A, B, C, D: 4 points that describe the upper trapezius
%       E, F, G, I: 4 points that describe the lower trapezius
%       H: Lower trapezius height
%
%   See also TRAPMF, DSIGMF, GAUSS2MF, GAUSSMF, GBELLMF, MF2MF, PIMF,
%   PSIGMF, SIGMF, SMF, TRIMF, ZMF, IT2GAUSSMF

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/07/19 $

if nargin ~= 2
    error('Two arguments are required by the trapezoidal MF.');
elseif length(params) < 9
    error('The interval type-2 trapezoidal MF needs at least nine parameters.');
end

a = params(1); b = params(2); c = params(3); d = params(4); e = params(5);
f = params(6); g = params(7); i = params(8); h = params(9);

if a > b,
    error('Illegal parameter condition: a > b');
elseif c > d,
    error('Illegal parameter condition: c > d');
elseif e > f
    error('Illegal parameter condition: e > f');
elseif g > i
    error('Illegal parameter condition: g > i');
elseif a > e
    error('Illegal parameter condition: a > e');
elseif a > f
    error('Illegal parameter condition: a > f');
elseif g > d
    error('Illegal parameter condition: g > d');
elseif i > d
    error('Illegal parameter condition: i > d');
elseif h > 1
    error('Illegal parameter condition: h > 1');
end

x = x(:);

y1 = zeros(size(x));
y2 = zeros(size(x));
y3 = zeros(size(x));
y4 = zeros(size(x));

% Compute y1
index = find(x >= b);
if ~isempty(index),
    y1(index) = ones(size(index));
end
index = find(x < a);
if ~isempty(index),
    y1(index) = zeros(size(index));
end
index = find(a <= x & x < b);
if ~isempty(index) && a ~= b,
    y1(index) = (x(index)-a)/(b-a);
end

% Compute y2
index = find(x <= c);
if ~isempty(index),
    y2(index) = ones(size(index));
end
index = find(x > d);
if ~isempty(index),
    y2(index) = zeros(size(index));
end
index = find(c < x & x <= d);
if ~isempty(index) && c ~= d,
    y2(index) = (d-x(index))/(d-c);
end

% Compute y3
index = find(x >= f);
if ~isempty(index),
    y3(index) = ones(size(index));
end
index = find(x < e);
if ~isempty(index),
    y3(index) = zeros(size(index));
end
index = find(e <= x & x < f);
if ~isempty(index) && e ~= f,
    y3(index) = (x(index)-e)/(f-e);
end

% Compute y4
index = find(x <= g);
if ~isempty(index),
    y4(index) = ones(size(index));
end
index = find(x > i);
if ~isempty(index),
    y4(index) = zeros(size(index));
end
index = find(g < x & x <= i);
if ~isempty(index) && g ~= i,
    y4(index) = (i-x(index))/(i-g);
end

y = zeros(length(x), 2);

% Compute y
y(:,1) = min(y1, y2);
y(:,2) = min(y3, y4) * h;