function y = it2gaussmf(x, params)
%IT2GAUSSMF Gaussian curve membership function.
%
%   IT2GAUSSMF(X, PARAMS) returns a matrix which is the Interval Type-2
%   Gaussian membership function evaluated at X. PARAMS is a 4-element
%   vector that determines the shape and position of this membership
%   function. The function accepts either equal sigma, with distinct
%   centers, the same centers and distinct sigma.
%
%   IT2GAUSSMF(X, [SIGMA1, C1, SIGMA2, C2]);
%   
%   See also GAUSSMF, DSIGMF, GAUSS2MF, GBELLMF, MF2MF, PIMF, PSIGMF,
%   SIGMF, SMF, TRAPMF, TRIMF, ZMF, IT2TRAPMF

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/07/19 $

if nargin ~= 2
    error('Two arguments are required by the Gaussian MF.');
elseif length(params) < 4
    error('The Interval Type-2 Gaussian MF needs at least four parameters.');
elseif params(1) == 0 || params(3) == 0,
    error('The Gaussian MF needs a non-zero sigma.');
end

if params(2) < params(4)
    sigma1 = params(1); c1 = params(2); sigma2 = params(3); c2 = params(4);
else
    sigma1 = params(3); c1 = params(4); sigma2 = params(1); c2 = params(2);
end

x = x(:);
f = zeros(length(x), 2);
y = zeros(length(x), 2);

f(:,1) = exp(-(x - c1).^2/(2*sigma1^2));
f(:,2) = exp(-(x - c2).^2/(2*sigma2^2));

y(:,1) = max([~(x < c1) & ~(x > c2) max(f, [], 2)], [], 2);
y(:,2) = min(f, [], 2);

end