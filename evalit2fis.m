function [output,IRR,ORR,ARR,TRR] = evalit2fis(input, fis, numofpoints)
%EVALIT2FIS Realiza os c�lculos do sistema de inferencia nebuloso Tipo-2 intervalar
%
%   [OUT, IRR, ORR, ARR, TRR] = EVALIT2FIS(INPUT, FIS, [NUMOFPOINTS])
%   simula o funcionamento sistema de inferencia nebuloso Tipo-2 Intervalar
%   FIS com para todas as entradas dadas em INPUT, discretizado em
%   NUMOFPOINTS pontos. Esta fun��o existe com prop�sitos de debug. Para
%   realizar os c�lculos use a fun��o EVALIT2FIS_MEX (mais eficiente).
%
%   Os parametros de entrada s�o:
%       INPUT: matriz M x N contendo entradas crisp. Sendo M o n�mero de
%       entradas e N o n�mero de vari�veis de cada entrada.
%       FIS: estrutura com as op��es do sistema fuzzy.
%       NUMOFPOINTS (opcional): n�mero de pontos para discretiza��o. O
%       valor padr�o � 101.
%
%   As sa�das s�o:
%       OUTPUT: sa�da defuzzificada das M entradas.
%       IRR: entradas fuzzificadas (Input Rule Result).
%       ARR: resultado da agrega��o por regra (Agregate Rule Result).
%       ORR: vetor das sa�das de cada regra (Output Rule Result).
%       TRR: resultado da tipo-redu��o (Type-Reduction Result).
%
%   Exemplo:
%       fis = readfis('analiseOleo');
%       it2fis = fis2it2fis(fis, 'it2trimf', 0.1);
%       out = evalit2fis([20 30; 340 900], it2fis)
%   Este c�digo gera a sa�da:
%       out =
%           0.8376
%           2.9615
%
%   See also FIS2IT2FIS, EVALIT2FIS_MEX.

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/07/25 $
%   $Based on: Kelly Liu's $  $Date: 1997/10/10 $


%% Check function inputs
ni = nargin;
if ni<2
    disp('Need at least two inputs');
    output=[];
    IRR=[];
    ORR=[];
    ARR=[];
    return
end

%% Check inputs
if ~isit2fis(fis)
    error('The second argument must be a FIS structure.')
end
[M,N] = size(input);
Nin = length(fis.input);
if M==1 && N==1, % Se for dado apenas um numero como entrada, esta ser� replicada para todas as entradas.
    input = input(:,ones(1,Nin));
elseif M==Nin && N~=Nin, % Se numero de linhas � igual ao numero de entradas e o numero de colunas n�o, transpoe
    input = input.';
elseif N~=Nin % Se os numero de colunas n�o for igual ao numero de entradas, a entrada � rejeitada.
    error('%s\n%s',...
        'The first argument should have as many columns as input variables and',...
        'as many rows as independent sets of input values.')
end

%% Check the fis for empty values
checkfis(fis);

%% Issue warning if inputs out of range
inRange = getfis(fis,'inRange');
InputMin = min(input,[],1);
InputMax = max(input,[],1);
if any(InputMin(:)<inRange(:,1)) || any(InputMax(:)>inRange(:,2))
    warning('Fuzzy:evalit2fis:InputOutOfRange','Some input values are outside of the specified input range.')
end

%% Compute output
if ni==2
    numofpoints = 101;
end

[output,IRR,ORR,ARR,TRR] = it2flsengine(input, fis, numofpoints, Nin, M);

end