function [outParams,errorStr]=mf2it2mf(inParams,inType,outType,uncertainty,minVal,maxVal,tol)
%MF2IT2MF Translates parameters between Type-1 to Type-2 membership functions.
%   
%   This function translates any built-in membership function
%   type into Interval Type-2 membership function, in terms of its
%   parameter set. mf2it2mf tries to mimic the symmetry points for both the
%   new and old membership functions. Occasionally this translation results
%   in lost information, so that if the output parameters are translated
%   back into the original membership function type, the transformed
%   membership function will not look the same as it did originally.
%
%   outParams = MF2IT2MF(inParams,inType,outType,uncertainty)
%
%   The input arguments for mf2mf are as follows: 
%       inParams: The parameters of the membership function you are
%       transforming
%       inType: a string name for the type of membership function you are
%       transforming
%       outType: a string name for the new membership function you are
%       transforming to. It accepts the following parameters:
%           'it2trapmf': returns an Interval Type-2 trapezoid function
%           'it2trimf': uses a it2trapmf to return IT2 triangular function
%           'it2gaussmf_us': returns Interval Type-2 gaussian with unequal
%           sigma
%           'it2gaussmf_um': returns Interval Type-2 gaussian with unequal
%           mean
%       uncertainty: an uncertainty measure to be added to the membership
%       functions
%
%   See also MF2MF.

%   Copyright 1994-2005 The MathWorks, Inc.
%   $Author: Ned Gulley $  $Date: 1994/06/17 $
%   $Modified by: Ciro Sobral $  $Date: 2014/08/18 $


yWaist=0.5;
yShoulder=0.90;
% outParams=[];
errorStr=[];
if strcmp(inType,'trimf'),
    lftWaist=yWaist*(inParams(2)-inParams(1))+inParams(1);
    lftShoulder=yShoulder*(inParams(2)-inParams(1))+inParams(1);
    rtShoulder=(1-yShoulder)*(inParams(3)-inParams(2))+inParams(2);
    rtWaist=(1-yWaist)*(inParams(3)-inParams(2))+inParams(2);

elseif strcmp(inType,'trapmf') || strcmp(inType,'pimf'),
    lftWaist=yWaist*(inParams(2)-inParams(1))+inParams(1);
    lftShoulder=yShoulder*(inParams(2)-inParams(1))+inParams(1);
    rtShoulder=(1-yShoulder)*(inParams(4)-inParams(3))+inParams(3);
    rtWaist=(1-yWaist)*(inParams(4)-inParams(3))+inParams(3);

elseif strcmp(inType,'gaussmf'),
    lftWaist=-abs(inParams(1))*sqrt(-2*log(yWaist))+inParams(2);
    lftShoulder=-abs(inParams(1))*sqrt(-2*log(yShoulder))+inParams(2);
    rtShoulder=2*inParams(2)-lftShoulder;
    rtWaist=2*inParams(2)-lftWaist;

elseif strcmp(inType,'gauss2mf'),
    lftWaist=-abs(inParams(1))*sqrt(-2*log(yWaist))+inParams(2);
    lftShoulder=inParams(2);
    rtShoulder=inParams(4);
    rtWaist=abs(inParams(3))*sqrt(-2*log(yWaist))+inParams(4);

elseif strcmp(inType,'gbellmf'),
    lftWaist=-inParams(1)*((1/yWaist-1)^(1/(2*inParams(2))))+inParams(3);
    lftShoulder=-inParams(1)*((1/yShoulder-1)^(1/(2*inParams(2))))+inParams(3);
    rtShoulder=2*inParams(3)-lftShoulder;
    rtWaist=2*inParams(3)-lftWaist;

elseif strcmp(inType,'sigmf'),
    if inParams(1)>0,
        lftWaist=inParams(2);
        lftShoulder=-1/inParams(1)*log(1/yShoulder-1)+inParams(2);
        rtShoulder=2*lftShoulder-lftWaist;
        rtWaist=2*rtShoulder-lftWaist;
    else
        rtWaist=inParams(2);
        rtShoulder=-1/inParams(1)*log(1/yShoulder-1)+inParams(2);
        lftShoulder=rtShoulder;
        lftWaist=2*lftShoulder-rtWaist;
    end

elseif strcmp(inType,'dsigmf'),
    lftWaist=inParams(2);
    lftShoulder=-1/inParams(1)*log(1/yShoulder-1)+inParams(2);
    rtWaist=inParams(4);
    rtShoulder=1/inParams(3)*log(1/yShoulder-1)+inParams(4);

elseif strcmp(inType,'psigmf'),
    lftWaist=inParams(2);
    lftShoulder=-1/inParams(1)*log(1/yShoulder-1)+inParams(2);
    rtWaist=inParams(4);
    rtShoulder=-1/inParams(3)*log(1/yShoulder-1)+inParams(4);

elseif strcmp(inType,'smf'),
    % lftWaist=yWaist*(inParams(2)-inParams(1))+inParams(1);
    lftShoulder=yShoulder*(inParams(2)-inParams(1))+inParams(1);
    rtShoulder=inParams(2);
    if inParams(1)<inParams(2),
        lftWaist=inParams(1);
        rtWaist=2*inParams(2)-inParams(1);
    else
        lftWaist=2*inParams(2)-inParams(1);
        rtWaist=inParams(1);
    end

elseif strcmp(inType,'zmf'),
    lftShoulder=inParams(2);
    rtShoulder=inParams(2);
    if inParams(1)<inParams(2),
        lftWaist=inParams(1);
        rtWaist=2*inParams(2)-inParams(1);
    else
        lftWaist=2*inParams(2)-inParams(1);
        rtWaist=inParams(1);
    end
else
    % Input MF type is unknown
    outParams=[];
    errorStr=['Cannot translate from input MF type ' inType];
    if nargout<2, error(errorStr); end
    return
end

% We've translated into generalized coordinates, now translate back into
% MF specific parameters...

if nargin<7,
    tol=max(eps, 1e-3*(rtShoulder-lftShoulder));
elseif nargin<6,
    maxVal = max(inParams);
    minVal = min(inParams);
end

delta=(maxVal-minVal)*(uncertainty/2);

if strcmp(outType,'it2trimf'),
    center=(rtShoulder+lftShoulder)/2;
    % Assumes yWaist=0.5
    outParams=[sort([2*lftWaist-center-delta center center 2*rtWaist-center+delta]) ...
        sort([2*lftWaist-center+delta center center 2*rtWaist-center-delta]) 1];

elseif strcmp(outType,'it2trapmf'),
    % Assumes yWaist=0.5
    
    outParams=[2*lftWaist-lftShoulder-delta lftShoulder-delta rtShoulder+delta 2*rtWaist-rtShoulder+delta ...
        2*lftWaist-lftShoulder+delta lftShoulder+delta rtShoulder-delta 2*rtWaist-rtShoulder-delta 1-uncertainty];
    
elseif strcmp(outType,'it2gaussmf_us'),
    center=(rtShoulder+lftShoulder)/2;
    sigma=max(tol,(rtWaist-center)/sqrt(-2*log(yWaist)));
    outParams=[sigma-delta center sigma+delta center];

elseif strcmp(outType,'it2gaussmf_um'),
    sigma=max(tol,(rtWaist-lftWaist)/4*sqrt(-2*log(yWaist)));
    outParams=[sigma lftShoulder-delta sigma rtShoulder+delta];

else
    % Output MF type is unknown
    outParams=[];
    errorStr=['Cannot translate to output MF type ' outType];
    if nargout<2, error(errorStr); end
    return
end

outParams=eval(mat2str(outParams,4));
