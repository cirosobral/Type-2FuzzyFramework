function h = plotIt2Mf(xaxis, yaxis, varargin)
%PLOTIT2MF Plota uma fun��o de pertin�ncia nebulosa tipo-2 intervalar.
%
%   Representa graficamente a �rea entre as fun��es de pertinencia superior
%   e inferior, sombreando uniformemente para indicar que todos os pontos
%   da fun��o de pertinencia secund�rias s�o uma unidade, considerando a
%   primeira coluna de Y como os valores superiores e a segunda coluna como
%   os valores inferiores.
%   
%   PLOTIT2MF(X, Y, ['name' str] ['color' vec|str] ['shade' val])
%       X: vetor com os valores da entrada crisp.
%       Y: matriz contendo os valores m�nimo (pimeira coluna) e m�ximo
%       (segunda coluna) da fun��o de pertinencia que se deseja plotar.
%       'name': string com o nome para a fun��o de pertin�ncia.
%       'color': aceita vetor com 3 valores de 0 a 1 (RGB), ou string
%       contendo o nome da cor para determinar a cor para a �rea sombreada.
%       'shade': escalar com o tom para a �rea sombreada.
%       'alpha': escalar indicando a transparencia da �rea sombreada.
%
%   See also PLOTMF, PLOTIT2VAR.

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/07/20 $

p = inputParser;

p.addParamValue('name', [], @(x) validateattributes(x, {'char'}, ...
    {'vector'}));
p.addParamValue('color', ones(1,3), @(x) checkColor(x));
p.addParamValue('shade', 1, @(x) validateattributes(x, {'double'}, ...
    {'scalar', '>=' 0, '<=', 1}));
p.addParamValue('alpha', 0.5, @(x) validateattributes(x, {'double'}, ...
    {'scalar', '>=' 0, '<=', 1}));

p.parse(varargin{:});

xaxis = xaxis(:);
upper = yaxis(:,1);
lower = yaxis(:,2);

xfill = [xaxis; flipud(xaxis)];
yfill = [upper; flipud(lower)];

if ischar(p.Results.color)
    color = p.Results.color;
else
    color = p.Results.color .* p.Results.shade;
end

if isempty(p.Results.name)
    h = patch(xfill,yfill, color);
else
    h = patch(xfill,yfill, color, 'DisplayName', p.Results.name);
    
    centerIndex=find(upper==max(upper));
    centerIndex=floor(mean(centerIndex));
    text(xaxis(centerIndex),1.05,p.Results.name, ...
        'HorizontalAlignment','center', 'VerticalAlignment','middle', ...
        'FontSize',10)
end

if any(strcmp('color',p.UsingDefaults))
    set(h,'FaceColor','flat',...
        'CData', randi(64,1),...
        'CDataMapping','scaled')
    set(h,'facealpha',p.Results.alpha);
end
ylim([-0.1 1.1]);
ylabel('Degree of membership','FontSize',10)
grid on

end

function checkColor(input)
    if ischar(input)
        validColors = {'y', 'm', 'c', 'r', 'g', 'b', 'k', 'w', ...
            'yellow', 'magenta', 'cyan', 'red',  ...
            'green', 'blue', 'white', 'black'};
        
        any(validatestring(input,validColors));
    else
        validateattributes(input, {'double'},{'numel', 3, ...
            '>=' 0, '<=', 1});
    end
end