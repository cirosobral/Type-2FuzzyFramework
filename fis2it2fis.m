function out = fis2it2fis(fis, mfType, uncertainty)
%FIS2IT2FIS Cria um FIS tipo-2 intervalar com base num FIS tipo-1.
%
%   FIS2IT2FIS(FIS, MFTYPE, UNCERTAINTY) retorna um IT2FIS (Sistema de
%   Inferencia Nebuloso Tipo-2 Intervalar) baseado num FIS, transformando
%   as fun��es de pertin�ncia da entrada em fun��es de pertin�ncia tipo-2
%   intervalares do tipo informado em MFTYPE. UNCERTAINTY representa uma 
%   medida da incerteza adicionada �s fun��es de pertin�ncia tipo-2.
%
%   Os valores esperados em MFTYPE s�o:
%       'it2trimf': As fun��es do IT2FIS ser�o representadas por 1
%       triangulo superior e 1 triangulo inferior;
%       'it2trapmf': As fun��es do IT2FIS ser�o representadas por 1
%       trap�zio superior e 1 trap�zio inferior;
%       'it2gauss_us': As fun��es do IT2FIS ser�o representadas por 2
%       gaussianas, com centros iguais e desvios distintos.
%       'it2gauss_um': As fun��es do IT2FIS ser�o representadas por 2
%       gaussianas, com desvios iguais e com centros distintos.

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/08/30 $

    switch mfType
        case 'it2trimf'
            mfRealType = 'it2trapmf';
        case {'it2gaussmf_us', 'it2gaussmf_um'}
            mfRealType = 'it2gaussmf';
        otherwise
            mfRealType = mfType;
    end
    
    out = fis;
    
    fisName=[fis.name '_it2'];
    andMethod=['op' upper(fis.andMethod(1)) fis.andMethod(2:end) 'It2'];
    orMethod=['op' upper(fis.orMethod(1)) fis.orMethod(2:end) 'It2'];
    
    if strcmp(fis.type, 'mamdani')
        fisType='it2ns'; % Sistema tipo-2 com sa�da n�o singleton
        impMethod='opMinIt2';
        aggMethod='opMaxIt2';
        typeRedMethod='ekm';
        defuzzMethod='wtaver';
    else
        fisType='it2s'; % Sistema tipo-2 com sa�da singleton
        impMethod=['op' upper(fis.impMethod(1)) fis.impMethod(2:end) 'It2'];
        aggMethod=['op' upper(fis.aggMethod(1)) fis.aggMethod(2:end) 'It2'];
        typeRedMethod='ekm';
        defuzzMethod='centroid';
    end
    
    out.name=fisName;
    out.type=fisType;
    out.andMethod=andMethod;
    out.orMethod=orMethod;
    out.typeRedMethod=typeRedMethod;
    out.defuzzMethod=defuzzMethod;
    out.impMethod=impMethod;
    out.aggMethod=aggMethod;
    
    for i=1:length(fis.input)
        range = fis.input(i).range;
        
        for j=1:length(fis.input(i).mf)
            out.input(i).mf(j).params = ...
                mf2it2mf(fis.input(i).mf(j).params, ...
                out.input(i).mf(j).type, mfType, uncertainty, range(1), ...
                range(2));
            
            out.input(i).mf(j).type = mfRealType;
        end
    end
    
end