function out=newit2fis(fisName)
%NEWIT2FIS Create new FIS.

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/07/19 $

if (nargin>=1), name=fisName; end
fisType='it2s';
andMethod='opProdIt2';
orMethod='opMaxIt2';
typeRedMethod='ekm';
defuzzMethod='centroid';
impMethod='opProdsIt2';
aggMethod='opMaxIt2';

out.name=name;
out.type=fisType;
out.andMethod=andMethod;
out.orMethod=orMethod;
out.typeRedMethod=typeRedMethod;
out.defuzzMethod=defuzzMethod;
out.impMethod=impMethod;
out.aggMethod=aggMethod;

% Create default values for the FIS structure input output and rule
out.input=[];
out.output=[];
out.rule=[];