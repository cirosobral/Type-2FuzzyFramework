function [output,IRR,ORR,ARR,TRR] = it2flsengine(input, fis, numofpoints, Nin, M)
%IT2FLSENGINE Realiza os c�lculos do sistema de inferencia nebuloso Tipo-2 intervalar
%
%   [OUT, IRR, ORR, ARR, TRR] = IT2FLSENGINE(INPUT, FIS, PTS, NIN, M)
%   simula o funcionamento sistema de inferencia nebuloso Tipo-2 Intervalar
%   FIS com NIN vari�veis de entrada, para as M entradas dadas em INPUT,
%   discretizado em PTS pontos. Esta fun��o N�O deve ser chamada
%   diretamente. Para realizar os c�lculos use a fun��o EVALIT2FIS_MEX.
%
%   Os parametros de entrada s�o:
%       INPUT: matriz contendo entradas crisp.
%       FIS: estrutura com as op��es do sistema fuzzy.
%       NUMOFPOINTS: n�mero de pontos para discretiza��o.
%       NIN: m�mero de vari�veis de entradas no sistema fuzzy.
%       M: n�mero de linhas das entradas crisp.
%
%   As sa�das s�o:
%       OUTPUT: sa�da defuzzificada.
%       IRR: entradas fuzzificadas (Input Rule Result).
%       ARR: resultado da agrega��o por regra (Agregate Rule Result).
%       ORR: vetor das sa�das de cada regra (Output Rule Result).
%       TRR: resultado da tipo-redu��o (Type-Reduction Result).
%
%   See also EVALIT2FIS, EVALIT2FIS_MEX.

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/11/30 $

maxMfs = 0;

for i = 1:Nin
    maxMfs = max(maxMfs, length(fis.input(i).mf));
end

x = zeros(numofpoints, Nin);
yMin = zeros(numofpoints, Nin, maxMfs);
yMax = zeros(numofpoints, Nin, maxMfs);

regras = length(fis.rule);

TRR = zeros(M, 2);

%% Fuzzifica��o

% Para cada entrada
for i = 1:Nin
    
    mfs = length(fis.input(i).mf);
    
    xMin = fis.input(i).range(1);
    xMax = fis.input(i).range(2);
    
    yTemp = zeros(numofpoints, 2);
    
    % Discretiza o espa�o de entrada
    x(:, i) = linspace(xMin, xMax, numofpoints)';
    
    % Transforma a entrada num �ndice das entradas no vetor X
    input(:, i) = round((input(:, i) - xMin) ./ ((xMax - xMin) ./ ...
        numofpoints)) + 1;
    
    % Para cada fun��o de pertinencia
    for j = 1:mfs % Para cada fun��o de pertinencia
        % O 'coder' n�o permite uso do 'str2func'.
        % func = str2func(fis.input(i).mf(j).type);
        mfType = fis.input(i).mf(j).type;
        
        if strcmp(mfType, 'it2gaussmf')
            % Para cada ponto na entrada, calcula o Y
            yTemp = it2gaussmf(x(:, i), fis.input(i).mf(j).params);
        elseif strcmp(mfType, 'it2trapmf')
            % Para cada ponto na entrada, calcula o Y
            yTemp = it2trapmf(x(:, i), fis.input(i).mf(j).params);
        end
        
        % Atribui o Y min e Y max
        yMax(:, i, j) = yTemp(:, 1);
        yMin(:, i, j) = yTemp(:, 2);
    end
end

% Trata os valores que excedam o m�ximo e o m�nimo na entrada.
input(input > numofpoints) = numofpoints;
input(input < 1) = 1;

% Trata os valores que tenham xMax - xMin = 0.
input(isnan(input)) = 1;

%% Motor de inferencia

andMethod = fis.andMethod;

IRR = zeros(regras, Nin, 2);
ARR = ones(regras, 2);
ORR = zeros(regras, 1);

% Aplica��o da conjun��o
if strcmp(andMethod, 'opProdIt2')
    for linha = 1:M % Para cada linha da entrada
        IRR = zeros(regras, Nin, 2);
        ARR = ones(regras, 2);
        ORR = zeros(regras, 1);
        
        for i = 1:regras
            ARR(i, 1) = fis.rule(i).weight;
            ARR(i, 2) = fis.rule(i).weight;

            params = fis.output.mf(fis.rule(i).consequent).params;
            tipo = fis.output.mf(fis.rule(i).consequent).type;

            if strcmp(tipo, 'constant')
                for j = 1:Nin
                    IRR(i, j, 1) = yMax(input(linha, j), j, fis.rule(i).antecedent(j));
                    IRR(i, j, 2) = yMin(input(linha, j), j, fis.rule(i).antecedent(j));

                    ARR(i, 1) = ARR(i, 1) .* IRR(i, j, 1);
                    ARR(i, 2) = ARR(i, 2) .* IRR(i, j, 2);
                end
            elseif strcmp(tipo, 'linear')
                for j = 1:Nin
                    IRR(i, j, 1) = yMax(input(linha, j), j, fis.rule(i).antecedent(j));
                    IRR(i, j, 2) = yMin(input(linha, j), j, fis.rule(i).antecedent(j));

                    ORR(i) = ORR(i) + x(input(linha, j), j) .* params(j);

                    ARR(i, 1) = ARR(i, 1) .* IRR(i, j, 1);
                    ARR(i, 2) = ARR(i, 2) .* IRR(i, j, 2);
                end
            end

            ORR(i) = ORR(i) + params(end);
        end
        
        % Aplica��o da Disjun��o
        % Encontra os consequentes �nicos
        [C, ia, ic] = unique(ORR);

        % Conta o n�mero de vezes que cada consequente aparece
        cont = histc(ORR, C);

        % Identifica os consquentes que aparecem mais de uma vez nas regras
        % ativadas, encontra o m�ximo e despreza os outros
        iaj = ia(cont > 1);

        aggMethod = fis.aggMethod;

        if strcmp(aggMethod, 'opMaxIt2')
            for j = 1:sum(cont > 1)
                maskMax = ic == ic(iaj(j));
                maskMin = maskMax;

                [~, idxMax] = max(ARR(maskMax, :));
                maskMax(idxMax(1)) = 0;
                maskMin(idxMax(2)) = 0;

                ARR(maskMax, 1) = 0;
                ARR(maskMin, 2) = 0;
            end
        elseif strcmp(aggMethod, 'opSumIt2')
            for j = 1:sum(cont > 1)
                maskMax = ic == ic(iaj(j));
                maskMin = maskMax;

                [~, idxMax] = max(ARR(maskMax, :));
                ARR(idxMax(1), 1) = sum(ARR(maskMax, 1));
                ARR(idxMax(2), 2) = sum(ARR(maskMin, 2));
                maskMax(idxMax(1)) = 0;
                maskMin(idxMax(2)) = 0;

                ARR(maskMax, 1) = 0;
                ARR(maskMin, 2) = 0;
            end
        end

        % Tipo-Redu��o
        TRR(linha, 1) = it2wtaver(ORR, ARR(:, 1), ARR(:, 2), 1);
        TRR(linha, 2) = it2wtaver(ORR, ARR(:, 1), ARR(:, 2), -1);
    end
elseif strcmp(andMethod, 'opMinIt2')
    for linha = 1:M % Para cada linha da entrada
        IRR = zeros(regras, Nin, 2);
        ARR = ones(regras, 2);
        ORR = zeros(regras, 1);
        
        for i = 1:regras
            ARR(i, 1) = fis.rule(i).weight;
            % ARR(i, 2) = fis.rule(i).weight;

            params = fis.output.mf(fis.rule(i).consequent).params;
            tipo = fis.output.mf(fis.rule(i).consequent).type;

            if strcmp(tipo, 'constant')
                for j = 1:Nin
                    IRR(i, j, 1) = yMax(input(linha, j), j, fis.rule(i).antecedent(j));
                    IRR(i, j, 2) = yMin(input(linha, j), j, fis.rule(i).antecedent(j));
                end
            elseif strcmp(tipo, 'linear')
                for j = 1:Nin
                    IRR(i, j, 1) = yMax(input(linha, j), j, fis.rule(i).antecedent(j));
                    IRR(i, j, 2) = yMin(input(linha, j), j, fis.rule(i).antecedent(j));

                    ORR(i) = ORR(i) + x(input(linha, j), j) .* params(j);
                end
            end

            ORR(i) = ORR(i) + params(end);
        end
        % A segunda coluna do ARR deve ser atualizada depois da primeira, 
        % pois o array de pesos s� foi inicializado na primeira coluna de 
        % ARR. Assim, pode ser usada a fun��o 'ones' para repetir a linha
        % da matriz.
        ARR(:, 2) = min(IRR(:, :, 2) .* ARR(:, ones(Nin, 1)), [], 2);
        ARR(:, 1) = min(IRR(:, :, 1) .* ARR(:, ones(Nin, 1)), [], 2);
        
        % Aplica��o da Disjun��o
        % Encontra os consequentes �nicos
        [C, ia, ic] = unique(ORR);

        % Conta o n�mero de vezes que cada consequente aparece
        cont = histc(ORR, C);

        % Identifica os consquentes que aparecem mais de uma vez nas regras
        % ativadas, encontra o m�ximo e despreza os outros
        iaj = ia(cont > 1);

        aggMethod = fis.aggMethod;

        if strcmp(aggMethod, 'opMaxIt2')
            for j = 1:sum(cont > 1)
                maskMax = ic == ic(iaj(j));
                maskMin = maskMax;

                [~, idxMax] = max(ARR(maskMax, :));
                maskMax(idxMax(1)) = 0;
                maskMin(idxMax(2)) = 0;

                ARR(maskMax, 1) = 0;
                ARR(maskMin, 2) = 0;
            end
        elseif strcmp(aggMethod, 'opSumIt2')
            for j = 1:sum(cont > 1)
                maskMax = ic == ic(iaj(j));
                maskMin = maskMax;

                [~, idxMax] = max(ARR(maskMax, :));
                ARR(idxMax(1), 1) = sum(ARR(maskMax, 1));
                ARR(idxMax(2), 2) = sum(ARR(maskMin, 2));
                maskMax(idxMax(1)) = 0;
                maskMin(idxMax(2)) = 0;

                ARR(maskMax, 1) = 0;
                ARR(maskMin, 2) = 0;
            end
        end

        % Tipo-Redu��o
        TRR(linha, 1) = it2wtaver(ORR, ARR(:, 1), ARR(:, 2), 1);
        TRR(linha, 2) = it2wtaver(ORR, ARR(:, 1), ARR(:, 2), -1);
    end
end

%% Defuzzifica��o

output = (TRR(:, 1) + TRR(:, 2)) ./ 2;

end