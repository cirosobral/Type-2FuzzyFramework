function tf = isit2fis(fis)
%ISIT2FIS Retorna 1 para estruturas IT2FIS, caso contrário retorna 0.

%   Copyright 2014 Ciro Sobral
%   $Author: Ciro Sobral $  $Date: 2014/07/04 $

tf = isstruct(fis) && all(ismember(...
        {'name'
        'type'
        'andMethod'
        'orMethod'
        'typeRedMethod'
        'defuzzMethod'
        'impMethod'
        'aggMethod'
        'rule'},fieldnames(fis)));
    
end